import { useState } from 'react';
import './App.css';

function App() {
  const [values, setValues] = useState({
    fisrtName:"",
    lastName:"",
    email:""
  });

  const [submited, setSubmited]= useState(false);

  function handleFristName(e){
    setValues({...values, fisrtName: e.target.value})
  }

  function handleLasttName(e){
    setValues({...values, lastName: e.target.value})
  }

  function handleEmail(e){
    setValues({...values, email: e.target.value})
  }

  function handleSubmit(e){
    e.preventDefault();
    setSubmited(true);
  }

  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        {submited ? <div className='success'>Success! Thank you for registering</div> : null}
        <label>
          <input type="text" placeholder='First Name' name="firstName" value={values.fisrtName} onChange={handleFristName}/>
          {!values.fisrtName ? <span>Please enter a first name</span> : null}
        </label>

        <label>
          <input type="text" placeholder='Last Name' name="lastName" value={values.lastName} onChange={handleLasttName}/>
          {!values.lastName ? <span>Please enter a last name</span> : null}
        </label>

        <label>
          <input type="email" placeholder='Email' name="email" value={values.email} onChange={handleEmail}/>
          {!values.email ? <span>Please enter a email</span> : null}
        </label>
        
        <input type="submit" value="Submit"/>
      </form>
    </div>
  );
}

export default App;
